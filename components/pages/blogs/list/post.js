import React, {useState} from 'react';
import Loading from '../../../ui/loading'
import AutoHeightWebView from 'react-native-autoheight-webview'

export default ({slug}) => {

    const [isLoading, setIsLoading] = useState(true);


    return (
        <>
            {isLoading && <Loading/>}
            <AutoHeightWebView
                source={{uri: `https://rchobby.pro/blogs/app/${slug}`}}
                scalesPageToFit={true}
                viewportContent={'width=device-width, user-scalable=no'}
                onLoad={() => setIsLoading(false)}
            />
        </>
    )

}
