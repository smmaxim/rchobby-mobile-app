import React, {useEffect, useState, useRef} from 'react';
import {StyleSheet, View, SafeAreaView, ScrollView} from 'react-native';
import {getItems, getItem} from './../../../../api/blogs'
import Card from './card'
import Post from './post'
import Loading from './../../../ui/loading'
import Container from './../../../ui/container'
import {Modalize} from 'react-native-modalize';
import {Portal} from 'react-native-portalize';

export default () => {

    const modalizeRef = useRef(null);
    const [blogs, setBlogs] = useState(null);
    const [blog, setBlog] = useState(null);

    useEffect(() => {
        handlerGetBlogs();
    }, []);

    const handlerGetBlogs = async ({nextUrl = null} = {}) => {
        let {data} = await getItems({nextUrl});
        if (nextUrl) data.items = [...blogs.items, ...data.items];
        setBlogs(data);
    };

    const handlerGetBlog = async ({slug}) => {
        setBlog(null);
        const {data} = await getItem(slug);
        setBlog(data);
        modalizeRef.current.open();
    };

    const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    const handlerScrollBottom = ({nativeEvent}) => {
        if (isCloseToBottom(nativeEvent) && blogs._links.next) {
            handlerGetBlogs({nextUrl: blogs._links.next.href})
        }
    };

    if (!blogs) {
        return <Loading/>
    }

    return (
        <View>

            <SafeAreaView style={styles.container}>
                <ScrollView
                    onScroll={handlerScrollBottom}
                    scrollEventThrottle={250}
                    style={styles.scrollView}
                >

                    {blogs.items
                        .map(item => (
                            <Card onClick={() => handlerGetBlog(item)} {...item} key={item.id}/>
                        ))}

                    {blogs && blogs._links.next && (<Loading/>)}

                </ScrollView>
            </SafeAreaView>

            <Portal>
                <Modalize ref={modalizeRef}>
                    {blog ? (<Post {...blog}/>) : (<Loading/>)}
                </Modalize>
            </Portal>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        marginBottom: 150
    }
});
