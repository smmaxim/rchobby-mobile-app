import React from 'react';
import {StyleSheet, View, ImageBackground, TouchableHighlight, Text} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';

export default (
    {
        title = '',
        small_text = '',
        imageUrl = '',
        user = {},
        slug = '',
        categories = [],
        onClick
    }
) => (
        <TouchableHighlight
            onPress={onClick}
            underlayColor="white"
        >
            <View style={styles.card}>

                <ImageBackground source={{uri: imageUrl}} style={styles.thumb}>
                    <LinearGradient
                        colors={['rgba(0,0,0,0.0)', 'rgba(0,0,0,0.0)', 'rgba(0,0,0,0.9)', '#313131']}
                        style={styles.thumbGradient}
                    >
                        <Text style={styles.title}>
                            {title}
                        </Text>
                    </LinearGradient>
                </ImageBackground>

                <Text style={styles.description}>
                    {small_text}
                </Text>

            </View>
        </TouchableHighlight>
)

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#313131',
        marginBottom: 20,
        borderRadius: 8,
        overflow: 'hidden'
    },
    title: {
        fontWeight: "bold",
        fontSize: 30,
        color: "#fff"
    },
    description: {
        padding: 20,
        color: '#C5C5C5',
        fontSize: 18,
        lineHeight: 26
    },
    thumb: {
        width: "100%",
        height: 280,
    },
    thumbGradient: {
        height: 100,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 20,
        paddingRight: 20,
        flex: 1,
        justifyContent: 'flex-end'
    }
});
