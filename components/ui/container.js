import React from 'react';
import {StyleSheet, View} from 'react-native';

export default ({children}) => (
    <View style={styles.container}>
        {children}
    </View>
)

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingLeft: 10,
        paddingRight: 10
    },
});
