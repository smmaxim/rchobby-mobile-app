import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Bounce} from 'react-native-animated-spinkit'

export default () => (
    <View style={styles.loading}>
        <Bounce size={36} color="#3f51b5"/>
    </View>
);

const styles = StyleSheet.create({
    loading: {
        alignItems: "center",
        flex: 1,
        marginTop: 20
    }
});
