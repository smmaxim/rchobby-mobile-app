import React from 'react';
import {StyleSheet, View} from 'react-native';
import Logo from './../../../../assets/logo'
import Container from './../../../ui/container'

export default () => (
    <View style={styles.header}>
        <Container>
            <Logo/>
        </Container>
    </View>
)

const styles = StyleSheet.create({
    header: {
        display: "flex",
        width: '100%',
        backgroundColor: '#313131',
        justifyContent: 'center',
        height: 62,
        marginBottom: 15
    },
});
