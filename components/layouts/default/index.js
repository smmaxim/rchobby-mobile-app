import React from 'react'
import Header from './header'
import Container from './../../ui/container'
import {Host} from 'react-native-portalize';

import {StyleSheet, View} from 'react-native';

export default ({children}) => (
    <Host>
        <View style={styles.container}>

            <Header style={styles.header}/>

            <Container>
                {children}
            </Container>

        </View>
    </Host>
)

const styles = StyleSheet.create({
    container: {},
    wrap: {}
});
