import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import axios from 'react-native-axios';

import DefaultLayout from './components/layouts/default';

import BlogListPage from './components/pages/blogs/list/';
import {StatusBar} from "react-native";

axios.defaults.baseURL = 'https://api.rchobby.pro';
const Stack = createStackNavigator();

export default function App() {
    return (
        <React.Fragment>
            <StatusBar hidden={true}/>
            <DefaultLayout>
                <BlogListPage/>
            </DefaultLayout>
        </React.Fragment>
    )
}

// <NavigationContainer>
//     <Stack.Navigator initialRouteName="Blog">
//         <Stack.Screen name="Blog" component={}/>
//     </Stack.Navigator>
// </NavigationContainer>
