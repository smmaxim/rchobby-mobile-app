import axios from 'react-native-axios';

export const getItems = ({nextUrl} = {}) => axios.get(nextUrl || '/blog/posts?expand=updated_at,user,categories');

export const getItem = (slug = '') => axios.get(`/blog/posts/view/${slug}?expand=meta_title ,meta_description ,meta_keywords, large_text,tags,comments`);
